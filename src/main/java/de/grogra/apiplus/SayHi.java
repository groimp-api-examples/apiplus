package de.grogra.apiplus;

import de.grogra.api.APIRunner;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Context;

public class SayHi{

    public static void appSayHi(Item item, Object info, Context ctx) throws Exception{
    	APIRunner a = (APIRunner)info;
    	String name=a.getParameter(APIRunner.INP_NAME, false);
    	a.addResponseContent(APIRunner.RETURN_DATA, "hi "+name);
    }

    public static void wbSayHi(Item item, Object info, Context ctx) throws Exception{
    	String wbName= ctx.getWorkbench().getName();
    	APIRunner a = (APIRunner)info;
    	String name=a.getParameter(APIRunner.INP_NAME, false);
    	a.addResponseContent(APIRunner.RETURN_DATA, "hi "+name +", best "+wbName);
    }
}